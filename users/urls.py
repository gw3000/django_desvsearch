from django.urls import path
from . import views


urlpatterns = [
    # all profiles
    path('', views.user_profiles, name='profiles'),
    # a single profile
    path('profile/<str:pk>/', views.user_profile, name='profile'),
    # login and register
    path('login', views.user_login, name='login'),
    # register
    path('register', views.user_register, name='register'),
    # logout
    path('logout', views.user_logout, name='logout'),
]
