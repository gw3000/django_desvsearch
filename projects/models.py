import os
import uuid
from datetime import date

from django.db import models
from django.db.models.deletion import CASCADE
from users.models import Profile


def project_file(instance, filename):
    """Renames uploaded project image to

    Args:
        instance ([type]): came from models.ImageField
        filename ([type]): came from models.ImageField

    Returns:
        string: project/%Y/%m/%d/str__str.ext
    """
    ext = filename.split('.')[-1]
    filename = f'{instance.pk}__{uuid.uuid4().hex}.{ext}'
    dirname = os.path.join('projects', date.today().strftime("%Y/%m/%d"))
    return os.path.join(dirname, filename)


class Project(models.Model):
    id = models.UUIDField(default=uuid.uuid4,
                          primary_key=True, unique=True, editable=False)
    title = models.CharField(max_length=30)
    description = models.TextField(null=True, blank=True)
    featured_image = models.ImageField(
        upload_to=project_file, default="default.jpg", null=True, blank=True)

    demo_link = models.CharField(max_length=2000, null=True, blank=True)
    source_link = models.CharField(max_length=2000, null=True, blank=True)
    tags = models.ManyToManyField('Tag', blank=True)
    # if the tag class sits beneath the projects class (which it does) then
    # you have to wrap the class Tag in single quotes.
    # If the class sits above, the you can leave the single quotes.
    vote_total = models.IntegerField(default=0, null=True, blank=True)
    vote_ratio = models.IntegerField(default=0, null=True, blank=True)
    created_at = models.DateField(auto_now_add=True)
    owner = models.ForeignKey(Profile, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.title


class Review(models.Model):
    VOTE_TYPE = (
        ('up', 'Up Vote'),
        ('down', 'Down Vote')
    )
    id = models.UUIDField(default=uuid.uuid4,
                          primary_key=True, unique=True, editable=False)
    # owner
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    body = models.TextField(null=True, blank=True)
    value = models.CharField(max_length=200, choices=VOTE_TYPE)
    created_at = models.DateField(auto_now_add=True)

    def __str__(self) -> str:
        return self.value


class Tag(models.Model):
    id = models.UUIDField(default=uuid.uuid4,
                          primary_key=True, unique=True, editable=False)
    name = models.CharField(max_length=200)
    created_at = models.DateField(auto_now_add=True)

    def __str__(self) -> str:
        return self.name
